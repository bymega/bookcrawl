<?php

namespace Models;

use SimpleOrm;

class Book extends SimpleOrm
{
    protected static $table = 'books';
}