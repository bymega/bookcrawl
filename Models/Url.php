<?php

namespace Models;

use SimpleOrm;

class Url extends SimpleOrm
{
    protected static $table = 'urls';
}