<?php

// Load Helper Files
$helperFiles = glob(dirname(__FILE__) . '/../Helpers/*.php');
foreach ($helperFiles as $helperFile) {
    require($helperFile);
}

// Connect DB
require(dirname(__FILE__) . '/../Includes/DbConn.php');

// Load Model Files
$modelFiles = glob(dirname(__FILE__) . '/../Models/*.php');
foreach ($modelFiles as $modelFile) {
    require($modelFile);
}

// Load Trait Class Files
$classTraitFiles = glob(dirname(__FILE__) . '/../Classes/Traits/*.php');
foreach ($classTraitFiles as $classTraitFile) {
    require($classTraitFile);
}

// Load Abstract Class Files
$classAbstractFiles = glob(dirname(__FILE__) . '/../Classes/Abstracts/*.php');
foreach ($classAbstractFiles as $classAbstractFile) {
    require($classAbstractFile);
}

// Load Class Files
$classFiles = glob(dirname(__FILE__) . '/../Classes/*.php');
foreach ($classFiles as $classFile) {
    require($classFile);
}

// Load Reader Class Files
$classReaderFiles = glob(dirname(__FILE__) . '/../Classes/Reader/*.php');
foreach ($classReaderFiles as $classReaderFile) {
    require($classReaderFile);
}

// Load Bots Class Files
$classBotFiles = glob(dirname(__FILE__) . '/../Classes/Bots/*.php');
foreach ($classBotFiles as $classBotFile) {
    require($classBotFile);
}