<?php

// Connect to the database using mysqli
$conn = new mysqli('127.0.0.1', 'root', 'by18mega.');

if ($conn->connect_error)
    die(sprintf('Unable to connect to the database. %s', $conn->connect_error));

// Tell SimpleOrm to use the connection you just created.
SimpleOrm::useConnection($conn, 'bookcrawl');