<?php

require 'Includes/Bootstrap.php';

$readers = [
    'jsonld' => new \Classes\Reader\JsonLdReader(),
    'microdata' => new \Classes\Reader\MicrodataReader(),
    'rdfa' => new \Classes\Reader\RdfaReader(),
];

// Çekim yapılacak bot
$botClass = new \Classes\Bots\StandaardBoekhandelBe();
$getLinks = $botClass->getLinks();

$x = 0;
foreach ($getLinks as $getLink) {
    $selectUrl = \Models\Url::retrieveByUrl($getLink, SimpleOrm::FETCH_ONE);
    if (isset($selectUrl->checked) && $selectUrl->checked == 0) {
        continue;
    }

    $bookReader = new \Classes\Reader\BookReader();
    $bookReader->setContent($getLink);

    $url = new \Models\Url;
    $url->url = $getLink;
    $url->checked = 0;
    $url->bot_id = $botClass->botId;

    $bookDetail = null;
    foreach ($readers as $readerKey => $reader) {
        $bookReader->read($reader);

        if ($bookReader->check()) {

            $replacePrice = str_replace(',', '.', $bookReader->getItem('price'));

            $bookDetail = [
                'id' => null,
                'reader' => $readerKey,
                'name' => $bookReader->getItem('name'),
                'description' => $bookReader->getItem('description'),
                'price' => $replacePrice,
                'currency' => $bookReader->getItem('priceCurrency'),
                'isbn' => $bookReader->getItem('isbn'),
                'review_count' => $bookReader->getItem('reviewCount'),
                'rating_value' => $bookReader->getItem('ratingValue'),
                'author' => $bookReader->getItem('author'),
                'image' => $bookReader->getItem('image')
            ];

            $url->checked = 1;
        }
    }

    if (! $selectUrl) {
        $url->save();
    }

    if ($bookDetail) {
        $bookDetail['url_id'] = $selectUrl ? $selectUrl->id : $url->id();

        $selectBook = \Models\Book::retrieveByIsbn($bookDetail['isbn'], SimpleOrm::FETCH_ONE);
        if (! $selectBook) {
            $book = new \Models\Book($bookDetail, 3);
        }
    }

    // deneme amaçlı ilk 5 kaydı ekliyorum
    $x++;
    if ($x > 4) {
        exit;
    }
}