<?php

namespace Classes\Abstracts;

use Classes\Traits\DomQueryTrait;

abstract class BotAbstract
{
    public $url, $linkQuery, $botId;
    public $addLinkPart = null;

    use DomQueryTrait;

    public function getLinks()
    {
        $getContent = helperContentFromUrl($this->url);

        $metaLinks = $this->xPath($getContent['content'])->query($this->linkQuery);
        foreach ($metaLinks->helperQuery as $metaLink) {
            $links[] = $this->addLinkPart ? $this->addLinkPart . $metaLink->getAttribute('href') : $metaLink->getAttribute('href');
        }

        return $links;
    }
}