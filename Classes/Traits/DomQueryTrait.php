<?php

namespace Classes\Traits;

use DOMDocument;
use DOMXPath;

trait DomQueryTrait
{
    public $helperDom, $helperXPath, $helperQuery;

    public function xPath($content)
    {
        $this->helperDom = new DOMDocument();
        @$this->helperDom->loadHTML($content);

        $this->helperXPath = new DomXPath($this->helperDom);

        return $this;
    }

    public function query($query)
    {
        $this->helperQuery = $this->helperXPath->query($query);

        return $this;
    }

    public function firstValue()
    {
        return $this->helperQuery->item(0) ? trim($this->helperQuery->item(0)->nodeValue) : null;
    }

    public function itemValue($item)
    {
        return $this->helperQuery->item($item) ? trim($this->helperQuery->item($item)->nodeValue) : null;
    }

    public function firstHtml()
    {
        return $this->helperDom->saveHtml($this->helperQuery->item(0));
    }

    public function itemHtml($item)
    {
        return $this->helperDom->saveHtml($this->helperQuery->item($item));
    }

    public function removeDomNodes($xpathString)
    {
        while ($node = $this->helperXPath->query($xpathString)->item(0)) {
            $node->parentNode->removeChild($node);
        }

        return $this;
    }
}