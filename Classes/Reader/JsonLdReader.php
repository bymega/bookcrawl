<?php

namespace Classes\Reader;

use DOMDocument;
use DOMXPath;

class JsonLdReader extends AbstractReader
{
    function check()
    {
        if ($this->getItem('name')) {
            return true;
        }

        return false;
    }

    function getItem($type)
    {
        $nodes = $this->query("//script[contains(.,'\"@type\":\"Book\"')]");
        if (! $nodes->firstValue()) {
            return null;
        }
        $firstItem = $nodes->firstValue() ?? null;

        $arrayItem = json_decode($firstItem, true);

        switch ($type) {
            case 'name':
                $searchArray = $this->search($arrayItem, '@type', 'Book');
                $searchValue = array_column($searchArray, 'name');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'description':
                $searchArray = $this->search($arrayItem, '@type', 'Book');
                $searchValue = array_column($searchArray, 'description');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'isbn':
                $searchArray = $this->search($arrayItem, '@type', 'Book');
                $searchValue = array_column($searchArray, 'isbn');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'reviewCount':
                $searchArray = $this->search($arrayItem, '@type', 'AggregateRating');
                $searchValue = array_column($searchArray, 'reviewCount');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'ratingValue':
                $searchArray = $this->search($arrayItem, '@type', 'AggregateRating');
                $searchValue = array_column($searchArray, 'ratingValue');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'price':
                $searchArray = $this->search($arrayItem, '@type', 'Offer');
                $searchValue = array_column($searchArray, 'price');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'priceCurrency':
                $searchArray = $this->search($arrayItem, '@type', 'Offer');
                $searchValue = array_column($searchArray, 'priceCurrency');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'author':
                $searchArray = $this->search($arrayItem, '@type', 'Person');
                $searchValue = array_column($searchArray, 'name');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;

            case 'image':
                $searchArray = $this->search($arrayItem, '@type', 'ImageObject');
                $searchValue = array_column($searchArray, 'url');

                if ($searchValue) {
                    return $searchValue[0];
                }
                break;
        }

        return null;
    }

    function search($array, $key, $value) {
        $results = array();

        if (is_array($array)) {

            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results,
                    $this->search($subarray, $key, $value));
            }
        }

        return $results;
    }
}