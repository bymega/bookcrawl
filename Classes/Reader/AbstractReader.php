<?php

namespace Classes\Reader;

use Classes\Traits\DomQueryTrait;

abstract class AbstractReader
{
    use DomQueryTrait;

    abstract function check();

    abstract function getItem($type);

    public function read($url)
    {
        $html = file_get_contents($url);

        return $this->xPath($html);
    }
}