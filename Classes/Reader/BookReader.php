<?php

namespace Classes\Reader;

use Classes\Traits\DomQueryTrait;

class BookReader
{
    public $reader, $content;

    public function setContent($url)
    {
        $this->content = file_get_contents($url);
    }

    public function read(AbstractReader $reader)
    {
        $this->reader = $reader;

        return $this->reader->xPath($this->content);
    }

    public function check()
    {
        return $this->reader->check();
    }

    public function getItem($type)
    {
        return $this->reader->getItem($type);
    }
}