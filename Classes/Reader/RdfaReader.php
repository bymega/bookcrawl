<?php

namespace Classes\Reader;

class RdfaReader extends AbstractReader
{
    public function check()
    {
        $check = $this->getItem('name');

        if ($check === null) {
            return false;
        }

        return true;
    }

    public function getItem($type)
    {
        $firstQuery = '//*[@typeof="Book"]';

        switch ($type) {
            case 'name':
                $query = $firstQuery . '//*[@property="name"]';
                break;

            case 'image':
                $query = $firstQuery . '//*[@property="image"]/@src';
                break;

            case 'author':
                $query = $firstQuery . '//*[@property="author"]';
                break;

            case 'isbn':
                $query = $firstQuery . '//*[@property="isbn"]';
                break;

            case 'ratingValue':
                $query = $firstQuery . '//*[@typeof="AggregateRating"]//*[@property="ratingValue"]';
                break;

            case 'reviewCount':
                $query = $firstQuery . '//*[@typeof="AggregateRating"]//*[@property="reviewCount"]';
                break;

            case 'price':
                $query = $firstQuery . '//*[@typeof="Offer"]//*[@property="price"]';
                break;

            case 'priceCurrency':
                $query = $firstQuery . '//*[@typeof="Offer"]//*[@property="priceCurrency"]/@content';
                break;

            default:
                return null;
        }

        $result =  $this->query($query)->firstValue();

        return $result;
    }
}