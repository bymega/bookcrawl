<?php

namespace Classes\Reader;

class MicrodataReader extends AbstractReader
{
    public function check()
    {
        $check = $this->getItem('name');

        if ($check === null) {
            return false;
        }

        return true;
    }

    public function getItem($type)
    {
        $firstQuery = '//*[@itemtype="https://schema.org/Book"]';

        switch ($type) {
            case 'name':
                $query = $firstQuery . '//*[@itemprop="name"]';
                break;

            case 'image':
                $query = $firstQuery . '//*[@itemprop="image"]/@src';
                break;

            case 'author':
                $query = $firstQuery . '//*[@itemprop="author"]';
                break;

            case 'isbn':
                $query = $firstQuery . '//*[@itemprop="isbn"]';
                break;

            case 'ratingValue':
                $query = $firstQuery . '//*[@itemtype="https://schema.org/AggregateRating"]//*[@itemprop="ratingValue"]';
                break;

            case 'reviewCount':
                $query = $firstQuery . '//*[@itemtype="https://schema.org/AggregateRating"]//*[@itemprop="reviewCount"]';
                break;

            case 'price':
                $query = $firstQuery . '//*[@itemtype="https://schema.org/Offer"]//*[@itemprop="price"]';
                break;

            case 'priceCurrency':
                $query = $firstQuery . '//*[@itemtype="https://schema.org/Offer"]//*[@itemprop="priceCurrency"]/@content';
                break;

            default:
                return null;
        }

        $result =  $this->query($query)->firstValue();

        return $result;
    }
}