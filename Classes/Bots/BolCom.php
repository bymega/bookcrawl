<?php

namespace Classes\Bots;

use Classes\Abstracts\BotAbstract;

class BolCom extends BotAbstract
{
    public function __construct()
    {
        $this->url = 'https://www.bol.com/nl/nl/l/boeken/8299/';
        $this->linkQuery = '//div[@class="product-title--inline"]/a';
        $this->addLinkPart = 'https://www.bol.com';
        $this->botId = 1;
    }
}