<?php

namespace Classes\Bots;

use Classes\Abstracts\BotAbstract;

class StandaardBoekhandelBe extends BotAbstract
{
    public function __construct()
    {
        $this->url = 'https://www.standaardboekhandel.be/c/boeken-ff70aa56';
        $this->linkQuery = '//article[@class="c-product"]/a';
        $this->addLinkPart = 'https://www.standaardboekhandel.be';
        $this->botId = 2;
    }
}