<?php

class BolComApi
{
    public function getToken()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://login.bol.com/token?grant_type=client_credentials',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic '. base64_encode("bcb1b867-8b93-4aed-82b9-7da96019bd0f:fJTJwjT0tWj-7sjhiks5Ul3mRCswFo4vQ4sMdiAABdVa-BD5jpF-LgAjvJye5a0Sg1PJUeareHoDf7YnQ0H8-A")
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function postOffers()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.bol.com/retailer/offers',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "ean": "0000000000000",
                "condition": {
                    "name": "MODERATE",
                    "category": "SECONDHAND",
                    "comment": "Description"
                },
                "reference": "YourReference",
                "onHoldByRetailer": false,
                "unknownProductTitle": "Additional text",
                "pricing": {
                    "bundlePrices": [
                        {
                            "quantity": 1,
                            "unitPrice": 9.99
                        },
                        {
                            "quantity": 6,
                            "unitPrice": 8,99
                        },
                        {
                            "quantity": 12,
                            "unitPrice": 7.99
                        }
                    ]
                },
                "stock": {
                    "amount": 1,
                    "managedByRetailer": false
                },
                "fulfilment": {
                    "method": "FBR",
                    "deliveryCode": "24uurs-21"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/vnd.retailer.v6+json',
                'Content-Type: application/vnd.retailer.v6+json',
                'Authorization: Bearer eyJraWQiOiJyc2EyIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJiY2IxYjg2Ny04YjkzLTRhZWQtODJiOS03ZGE5NjAxOWJkMGYiLCJhenAiOiJiY2IxYjg2Ny04YjkzLTRhZWQtODJiOS03ZGE5NjAxOWJkMGYiLCJjbGllbnRuYW1lIjoiUHJvZ29uQm9sIiwiaXNzIjoiaHR0cHM6XC9cL2xvZ2luLmJvbC5jb20iLCJzY29wZXMiOiJSRVRBSUxFUiIsImV4cCI6MTYzOTA4NDAxNSwiaWF0IjoxNjM5MDgzNzE1LCJhaWQiOiJDTE5UQzpiZTJiNzAxNi03MzQ2LWMzNmUtZDEzOC03NzcwODE3M2Y3YmMgU0xSOjE3MDczODIiLCJqdGkiOiI5NGE1YThmYi02NmUyLTQ4OTMtYmVmMy0wNTAyMTBkNTBlYzUifQ.J5VC85EIdSI_wRYEosN6MegP2uL-wLtgpKQMhd5Atodwz0MCEN2Basnhl71buEv0x_Y_fs9eQZyPMyxd8alU_60FLgs1nI4DmoXshts81Lefr_SHegxfGbUbrCgpyVi7_RqqhWjWq7mZMtmL4QISHCWWwujQUXtHUU6llvtRFWe21RjXn2iaQav3FQSZroHPWk3OlJJex3ul13SDL4JndXt9cCjC2mTOUiBux7S_bkGyqOv0-4oZRRhZ_qYj7yCB651kKdjR9bB6ks_ipqLIhVMb176WSUKbH_vx1XW7mXOx0LX2i_ovaRt69A5j7tQhN4Cpgm0ZYs1Rdah-kYGWiA'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}